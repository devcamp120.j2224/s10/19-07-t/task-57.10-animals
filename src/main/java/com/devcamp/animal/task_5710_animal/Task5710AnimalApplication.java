package com.devcamp.animal.task_5710_animal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5710AnimalApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task5710AnimalApplication.class, args);
	}

}
